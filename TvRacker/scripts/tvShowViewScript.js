var mySeriesArr = [];

$('#seriesOverviewPage').on('pageinit', function() {    
    var currentTVShowObj = JSON.parse(localStorage['currentTVShow']);
    var alreadyAdded = false;
    
    if (localStorage.mySeries.length > 0) {
        var myAddedSeries = JSON.parse(localStorage.mySeries);
    
        for (i = 0; i < myAddedSeries.length; i++) {
            if (currentTVShowObj.id == myAddedSeries[i].id) {
                alreadyAdded = true;
                break;
            }
        }
        if (alreadyAdded) {
            $('#addButLI').remove();
        }
    }
    
    putBGImageToJumbotron(currentTVShowObj);
    putOverviewOnScreen(currentTVShowObj);
});

function putBGImageToJumbotron(currentTVShowObj) {
    APIKey = 'cedf657c120ecb9c0785cccf3f5fa293';
    imageUrl = '';
    
    if (currentTVShowObj.backdrop_path != null) {
        imageUrl = 'http://image.tmdb.org/t/p/w500' + currentTVShowObj.backdrop_path;
    } else {
        imageUrl = 'http://icoconvert.com/images/noimage.png';
    }
    
    $('#overviewPictureJumbo').css('background-image', 'url(' + imageUrl + ')');
}

function putOverviewOnScreen(currentTVShowObj) {
    var currentTVShowObjName = "";
    var currentTVShowObjVoteAvg = "";
    var currentTVShowObjVoteCount = "";
    var currentTVShowObjOverView = "";
    var myFormattedFirstAir = "";
    
    if (currentTVShowObj.name != undefined) {
        currentTVShowObjName = currentTVShowObj.name; 
    }
    if (currentTVShowObj.vote_average != undefined) {
        currentTVShowObjVoteAvg = currentTVShowObj.vote_average; 
    }
    if (currentTVShowObj.vote_count != undefined) {
        currentTVShowObjVoteCount = currentTVShowObj.vote_count; 
    }
    if (currentTVShowObj.overview != undefined) {
        currentTVShowObjOverView = currentTVShowObj.overview; 
    }
    if (currentTVShowObj.first_air_date != undefined) {
        var date2 = new Date(currentTVShowObj.first_air_date);
        myFormattedFirstAir = date2.getDate() + "/" + date2.getMonth() + "/" +date2.getFullYear();
    }
    
    name = '<li>'+'<b>Name: </b>'+ currentTVShowObj.name +'</li>';
    ranking = '<li>'+'<b>Average ranking: </b>'+ currentTVShowObj.vote_average +'</li>';
    rankCount = '<li>'+'<b>Ranking count: </b>'+ currentTVShowObj.vote_count +'</li>';
    overview = '<li>'+'<b>Overview: </b>'+ currentTVShowObj.overview +'</li>';
    firstAir = '<li>'+'<b>First air date: </b>'+ myFormattedFirstAir +'</li>';
    
    
    $('#seriesInfo').append(name);
    $('#seriesInfo').append(ranking);
    $('#seriesInfo').append(rankCount);
    $('#seriesInfo').append(overview);
    $('#seriesInfo').append(firstAir);
}

function addShow() {
    if (typeof(localStorage) != 'undefined') {
        var found = false;
        if (localStorage.mySeries.length > 0) {
            mySeriesArr = JSON.parse(localStorage.mySeries);
            
            for (i = 0; i < mySeriesArr.length; i++) {
                currentTVShowObj = JSON.parse(localStorage.currentTVShow);
                if(mySeriesArr[i].id == currentTVShowObj.id) {
                    alert("Already added, soz, wont add again.");
                    found = true;
                    break;
                }
            }
        }
        if (!found) {
            localStorage.mySeries = [];
            mySeriesArr.push(JSON.parse(localStorage.currentTVShow));
            localStorage.setItem('mySeries', JSON.stringify(mySeriesArr));
        } 
    }
    $(location).attr('href', 'index.html')
}

function episodeViewClick() {  
}