var TVShowListCounter = 0;
var popularTVShows = [];

$('#popularSeriesPage').on('pageshow',function(){
    if (localStorage['lastSearchResults'] != undefined) {
        localStorage['lastSearchResults'] = [];   
    }
    showPopularTVSeries();
    TVShowListCounter = 0;
});

function showPopularTVSeries() {
    var APIKey = 'cedf657c120ecb9c0785cccf3f5fa293'
    var url = 'https://api.themoviedb.org/3/tv/popular?api_key=' + APIKey;
    
    
    $("#popularSeries").empty();
    $.get(url, function(data) {  
        for (i = 0; i < data.results.length; i++) {
            popularTVShows.push(data.results[i]);
        }    
    }).done(function () {
        getCurrentImageFileFullPathAndShow(popularTVShows, APIKey);
    });
};

function getCurrentImageFileFullPathAndShow(popularTVShows, APIKey) {
    imageUrlBase = 'http://api.themoviedb.org/3/tv/';
    imageFilePathBase = 'http://image.tmdb.org/t/p/w500';
    imageFilePath = '';
    
    for (i = 0; i < popularTVShows.length; i++) {
        imageUrl = imageUrlBase + popularTVShows[i].id + '/images?api_key=' + APIKey;
        
        $.get(imageUrl, function(imageData) {
            imageFilePath += imageFilePathBase + imageData.backdrops[0].file_path;
        }).done(function () {
            showOutInList(popularTVShows[TVShowListCounter], imageFilePath);
            imageFilePath = '';
        });
    }
}
function showOutInList(currentTVShow, imageFilePath) {
    TVShowListCounter++;
    
    var currentTVShowName = currentTVShow.name;

    listItem = '<li id="popularSeriesItem" style="background-image:url('+imageFilePath+')"><a id="findSeriesShows" onclick="overViewPageClicked(\'' + currentTVShowName + '\')" href="overView.html">'+currentTVShowName+'</a></li>';
    
    $("#popularSeries").append(listItem);
}

function overViewPageClicked(currentTVShowName) {
    
    for (i = 0; i < popularTVShows.length; i++) {
        popularTVShows[i].name = popularTVShows[i].name.replace("'",'');
        if (popularTVShows[i].name == currentTVShowName) {
            var currentTVShowObj = popularTVShows[i];
            
            if (typeof(localStorage) != 'undefined') {
                localStorage['currentTVShow'] = JSON.stringify(currentTVShowObj);    
            }
        }
    }
}

function mySeriesViewClicked() {
    $(location).attr('href', 'index.html'); 
}
