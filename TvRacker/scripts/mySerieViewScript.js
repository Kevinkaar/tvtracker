$('#mySeriesPage').on('pageinit', function() {
    if (typeof(localStorage) != 'undefined') {
        if (localStorage['mySeries'] != undefined) {
            var mySerieObjects = JSON.parse(localStorage.mySeries);
            getMySeriesImgPathAndShow(mySerieObjects);
        } else {
            localStorage['mySeries'] = [];   
        }
    }
});

function getMySeriesImgPathAndShow(mySerieObjects) {
    $("#mySeries").empty();
    imageFilePathBase = 'http://image.tmdb.org/t/p/w500';
    
    for (i = 0; i < mySerieObjects.length; i++) {
        if (mySerieObjects[i].backdrop_path != null) {
            imageUrl = imageFilePathBase + mySerieObjects[i].backdrop_path;   
        } else {
            imageUrl = 'http://icoconvert.com/images/noimage.png'
        }
        showOutInList(mySerieObjects[i], imageUrl);
    }
}

function showOutInList(currentTVShow, imageFilePath) {
    var currentTVShowName = currentTVShow.name;

    listItem = '<li id="popularSeriesItem" style="background-image:url('+imageFilePath+')"><a id="mySeriesTVShow" onclick="episodeViewPageClicked(\'' + currentTVShowName + '\')" href="overView.html">'+currentTVShowName+'</a></li>';
    
    $("#mySeries").append(listItem);
}

function episodeViewPageClicked(currentTVShowName) {
    mySeries = JSON.parse(localStorage.mySeries);
    for (i = 0; i < mySeries.length; i++) {
        mySeries[i].name = mySeries[i].name.replace("'",'');
        if (mySeries[i].name == currentTVShowName) {
            var currentTVShowObj = mySeries[i];
            
            if (typeof(localStorage) != 'undefined') {
                localStorage['currentTVShow'] = JSON.stringify(currentTVShowObj);    
            }
        }
    }
}