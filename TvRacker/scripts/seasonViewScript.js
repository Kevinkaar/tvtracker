var seasonCounter = 0;
var currentTVShowSeason = [];

$('#seasonViewPage').on('pageinit', function() {
    if (typeof(localStorage) != 'undefined') {
        var currentTVShowObj = JSON.parse(localStorage.currentTVShow);
        setCurrentTVShowSeasons(currentTVShowObj);
    }
});

function setCurrentTVShowSeasons(currentTVShowObj) {
    $("#seasons").empty();
    var currentTVShowID = currentTVShowObj.id;
    var APIKey = 'cedf657c120ecb9c0785cccf3f5fa293';

    var url = "http://api.themoviedb.org/3/tv/" + currentTVShowID 
                + "/season/"+ seasonCounter + "?api_key=" + APIKey;

    getAmountOfSeasons(currentTVShowObj, APIKey, url);
}

function getAmountOfSeasons(currentTVShowObj, APIKey, seasonsUrl) {
    var url = "http://api.themoviedb.org/3/tv/" + currentTVShowObj.id + 
                "?api_key=" + APIKey;
    var amountOfSeasons = ''
    
    $.get(url, function(data) {
        console.log(url);
       amountOfSeasons = data.seasons.length; 
    }).done(function () {
        getSeasonInformation(url, APIKey, currentTVShowObj, amountOfSeasons)
    });
    
}

function getSeasonInformation(url, APIKey, currentTVShowObj, amountOfSeasons) {
    seasonCounter++;

    if (amountOfSeasons >= seasonCounter) {
        $.get(url, function(data) {
            currentTVShowSeason.push(data);
            url = "http://api.themoviedb.org/3/tv/" + currentTVShowObj.id 
                + "/season/"+ seasonCounter + "?api_key=" + APIKey;
            getSeasonInformation(url, APIKey, currentTVShowObj, amountOfSeasons);
        });
    } else {
        showSeasons(currentTVShowObj);
    }
}

function showSeasons(currentTVShowObj) {
    var currentTVShowName = currentTVShowObj.name;
    
    for (i = 1; i < currentTVShowSeason.length; i++) {
        imageFilePath = "http://image.tmdb.org/t/p/w500" + currentTVShowSeason[i].poster_path;
        
        listItem = '<li id="seasonItems" style="background-image:url('+imageFilePath+')"><a id="findSeriesShows" onclick="tvShowSeasonClicked(\'' + currentTVShowSeason[i].season_number + '\')" href="episodeView.html">'+currentTVShowName+ " - " + currentTVShowSeason[i].name +'</a></li>';
        
        $("#seasons").append(listItem);
    }
}

function tvShowSeasonClicked(currentTVSeasonNumber) {
    
    if (typeof(localStorage) != 'undefined') {
        if (currentTVSeasonNumber == undefined) {
            currentTVSeasonNumber = 0;   
        }
        localStorage['currentSeason'] = JSON.stringify(currentTVSeasonNumber);
    }
  
}