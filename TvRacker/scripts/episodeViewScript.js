$('#episodeViewPage').on('pageinit', function() {
    if (typeof(localStorage) != 'undefined') {
        if (localStorage['seenEpisodeObjects'] == undefined) {
            localStorage['seenEpisodeObjects'] = [];
        }
        var currentTVShowObj = JSON.parse(localStorage.currentTVShow);
        setCurrentTVShowEpisodes(currentTVShowObj);
    }
});

function setCurrentTVShowEpisodes(currentTVShowObj) {
    $("#episodes").empty();
    var currentTVShowID = currentTVShowObj.id;
    var APIKey = 'cedf657c120ecb9c0785cccf3f5fa293';
    var currentSeasonNO = localStorage.currentSeason.replace("\"","");

    var url = "http://api.themoviedb.org/3/tv/" + currentTVShowID 
                + "/season/"+ currentSeasonNO + "?api_key=" + APIKey;
    
    var currentSeasonObj;
    
    $.get(url, function(data) {
        currentSeasonObj = data;
    }).done(function () {
        showEpisodes(currentSeasonObj);
    });
}

function showEpisodes(currentSeasonObj) {
    var currentTVShowName = JSON.parse(localStorage.currentTVShow).name;
    var hasSeenEpisodeObjects = localStorage.seenEpisodeObjects.length > 0;
    if (hasSeenEpisodeObjects) {
        var seenEpisodeObjects = JSON.parse(localStorage.seenEpisodeObjects);
    }
    
    
    for (i = 0; i < currentSeasonObj.episodes.length; i++) {
        var foundSeen = false;
        if (currentSeasonObj.episodes[i].still_path == undefined) {
            imageFilePath = 'http://icoconvert.com/images/noimage.png'
        } else {
            imageFilePath = "http://image.tmdb.org/t/p/w500" + currentSeasonObj.episodes[i].still_path;
        }
        if (hasSeenEpisodeObjects) {
            for (j = 0; j < seenEpisodeObjects.length; j++) {
                if (seenEpisodeObjects[j].id == currentSeasonObj.episodes[i].id) {
                    foundSeen = true;   
                }
            }
        }
        
        var listItem;
        
        if (foundSeen) {
            listItem = '<li id="seasonItems" style="background-image:url('+imageFilePath+')"><a id="mySeriesTVShow"  onclick="tvShowEpisodeClicked(\'' + currentSeasonObj.episodes[i].episode_number + '\')" href="episodeDetailView.html">'+currentTVShowName+ " - " + currentSeasonObj.episodes[i].name +'</a></li>';
        } else {
            listItem = '<li id="seasonItems" style="background-image:url('+imageFilePath+')"><a id="tvShowLink"  onclick="tvShowEpisodeClicked(\'' + currentSeasonObj.episodes[i].episode_number + '\')" href="episodeDetailView.html">'+currentTVShowName+ " - " + currentSeasonObj.episodes[i].name +'</a></li>';
        }

        $("#episodes").append(listItem);
    }
}

function tvShowEpisodeClicked(currentTVEpisodeNO) {
       if (typeof(localStorage) != 'undefined') {
            if (currentTVEpisodeNO == undefined) {
                currentTVEpisodeNO = 0;   
            }
        localStorage['currentEpisode'] = JSON.stringify(currentTVEpisodeNO);
    }
}

