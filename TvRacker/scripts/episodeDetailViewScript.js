var seenEpisodeArr = [];

$('#episodeDetailPage').on('pageinit', function() {
    if (typeof(localStorage) != 'undefined') {
        var currentTVShowObj = JSON.parse(localStorage.currentTVShow);
        setCurrentTVShowInfo(currentTVShowObj);
    }
});

function addEpisodeToSeen() {
    var isDuplicate = false;
    if (typeof(localStorage) != 'undefined') {
        if (localStorage.seenEpisodeObjects.length > 0) {
            seenEpisodeArr = JSON.parse(localStorage.seenEpisodeObjects);
            localStorage.seenEpisodeObjects = []; 
        }
        for (i = 0; i < seenEpisodeArr.length; i++) {
            if (seenEpisodeArr[i].id == JSON.parse(localStorage.currentEpisodeObject).id) {
                isDuplicate = true;
                alert("already added to seen!")
            }
        }
        if (!isDuplicate) {
            seenEpisodeArr.push(JSON.parse(localStorage.currentEpisodeObject));
        }  
        localStorage.setItem('seenEpisodeObjects', JSON.stringify(seenEpisodeArr));
    }
}

function putBGImageToJumbotron(currentTVShowObj, currentEpisodeObj) {
    APIKey = 'cedf657c120ecb9c0785cccf3f5fa293';
    imageUrl = '';
    
    if (currentEpisodeObj.still_path != null) {
        imageUrl = 'http://image.tmdb.org/t/p/w500' + currentEpisodeObj.still_path;
    } else {
        imageUrl = 'http://icoconvert.com/images/noimage.png';
    }
    
    $('#overviewPictureJumbo').css('background-image', 'url(' + imageUrl + ')');
}

function setCurrentTVShowInfo(currentTVShowObj) {
    $("#episodeInfo").empty();
    var currentTVShowID = currentTVShowObj.id;
    var APIKey = 'cedf657c120ecb9c0785cccf3f5fa293';
    var currentSeasonNO = localStorage.currentSeason.replace("\"","");
    var currentEpisodeNO = localStorage.currentEpisode.replace("\"","");
    
    var url = "http://api.themoviedb.org/3/tv/" + currentTVShowID +
                "/season/" + currentSeasonNO + "/episode/" +
                currentEpisodeNO + "?api_key=" + APIKey;
    
    var currentEpisodeObj;
    
    $.get(url, function(data) {
        currentEpisodeObj = data;
    }).done(function () {
        showEpisodeInfo(currentEpisodeObj);
        putBGImageToJumbotron(currentTVShowObj, currentEpisodeObj);
    });
}

function showEpisodeInfo(currentEpisodeObj) {    
    localStorage['currentEpisodeObject'] = JSON.stringify(currentEpisodeObj);
    
    var currentEpisodeObjName = "";
    var currentEpisodeObjSeason = "";
    var currentEpisodeObjEpisode = "";
    var currentEpisodeObjVoteAvg = "";
    var currentEpisodeObjVoteCount = "";
    var currentEpisodeObjOverView = "";
    var myFormattedFirstAir = "";
    
    if (currentEpisodeObj.name != undefined) {
        currentEpisodeObjName = currentEpisodeObj.name; 
    }
    if (currentEpisodeObj.season_number != undefined) {
        currentEpisodeObjSeason = currentEpisodeObj.season_number; 
    }
    if (currentEpisodeObj.episode_number != undefined) {
        currentEpisodeObjEpisode = currentEpisodeObj.episode_number; 
    }
    if (currentEpisodeObj.vote_average != undefined) {
        currentEpisodeObjVoteAvg = currentEpisodeObj.vote_average; 
    }
    if (currentEpisodeObj.vote_count != undefined) {
        currentEpisodeObjVoteCount = currentEpisodeObj.vote_count; 
    }
    if (currentEpisodeObj.overview != undefined) {
        currentEpisodeObjOverView = currentEpisodeObj.overview; 
    }
    if (currentEpisodeObj.first_air_date != undefined) {
        var date2 = new Date(currentEpisodeObj.first_air_date);
        myFormattedFirstAir = date2.getDate() + "/" + date2.getMonth() + "/" +date2.getFullYear();
    }

    
    name = '<li>'+'<b>Name: </b>'+ currentEpisodeObj.name +'</li>';
    season = '<li>'+'<b>Season: </b>'+ currentEpisodeObj.season_number +'</li>';
    episode = '<li>'+'<b>Episode: </b>'+ currentEpisodeObj.episode_number +'</li>';
    ranking = '<li>'+'<b>Average ranking: </b>'+ currentEpisodeObj.vote_average +'</li>';
    rankCount = '<li>'+'<b>Ranking count: </b>'+ currentEpisodeObj.vote_count +'</li>';
    overview = '<li>'+'<b>Overview: </b>'+ currentEpisodeObj.overview +'</li>';
    firstAir = '<li>'+'<b>First air date: </b>'+ myFormattedFirstAir +'</li>';
    
    $('#episodeInfo').append(name);
    $('#episodeInfo').append(season);
    $('#episodeInfo').append(episode);
    $('#episodeInfo').append(ranking);
    $('#episodeInfo').append(rankCount);
    $('#episodeInfo').append(overview);
    $('#episodeInfo').append(firstAir);
}
