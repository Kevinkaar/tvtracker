var searchResultTVSeries = [];
var lastSearchResultsArr = [];

$('#discoverSeriesPage').on('pageinit', function() {
    if (typeof(localStorage) != 'undefined') {
        if (localStorage['lastSearchResults'] == undefined) {
            localStorage['lastSearchResults'] = []; 
        } else {
            if (localStorage.lastSearchResults.length > 0) {
                lastSearchResultsArr = JSON.parse(localStorage.lastSearchResults);
                getCurrentSeriesPathAndShow(lastSearchResultsArr);
            }
        }
    }
});

$("#searchForm").keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
        searchSeries();
        event.preventDefault();
        return false;
	}
});

function searchSeries() {
    
    var APIKey = 'cedf657c120ecb9c0785cccf3f5fa293';
    var tvShowQuery = $("#searchSeriesInput").val();
    var url = 'https://api.themoviedb.org/3/search/tv?api_key=' + APIKey + '&query=' + tvShowQuery;
    
    if (tvShowQuery != "") {
        $.get(url, function(data) {
            $("#searchResults").empty();
            searchResultTVSeries = [];
            for (i = 0; i < data.results.length; i++) {
                searchResultTVSeries.push(data.results[i]);
            }
        }).done(function () {
            getCurrentSeriesPathAndShow(searchResultTVSeries);
            localStorage.lastSearchResults = JSON.stringify(searchResultTVSeries);
        });   
    }
}

function getCurrentSeriesPathAndShow(searchResult) {
    imageFilePathBase = 'http://image.tmdb.org/t/p/w500';
    
    for (i = 0; i < searchResult.length; i++) {
        if (searchResult[i].backdrop_path != null) {
            imageUrl = imageFilePathBase + searchResult[i].backdrop_path;   
        } else {
            imageUrl = 'http://icoconvert.com/images/noimage.png'
        }
        showOutInList(searchResult[i], imageUrl);
    }
}

function showOutInList(currentTVShow, imageFilePath) {
    var currentTVShowName = currentTVShow.name;
    var currentTVShowID = currentTVShow.id;

    listItem = '<li id="popularSeriesItem" style="background-image:url('+imageFilePath+')"><a id="findSeriesShows" onclick="overViewPageClicked(\'' + currentTVShowID + '\')" href="overView.html">'+currentTVShowName+'</a></li>';
    
    $("#searchResults").append(listItem);
}

function overViewPageClicked(currentTVShowID) {
    var lastSearchRes = JSON.parse(localStorage.lastSearchResults);
    for (i = 0; i < lastSearchRes.length; i++) {
        if (lastSearchRes[i].id == currentTVShowID) {
            var currentTVShowObj = lastSearchRes[i];
            
            if (typeof(localStorage) != 'undefined') {
                localStorage['currentTVShow'] = JSON.stringify(currentTVShowObj);
            }
        }
    }
}

function mySeriesViewClicked() {
    $(location).attr('href', 'index.html');   
}
